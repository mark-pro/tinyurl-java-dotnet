package com.url.tiny.tinyurl.services;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.url.tiny.tinyurl.models.TinyUrl;

@Component
public class TinyWorkerService implements TinyService {
    
    static final HashMap<String, String> urls = new HashMap<>();

    @Override
    public void save(TinyUrl tinyUrl) {
        urls.put(tinyUrl.getId(), tinyUrl.getLongUrl());
    }

    @Override
    public TinyUrl generateTinyUrl(String longUrl) {
        var id = Base62.create(7);
        return new TinyUrl(id, longUrl);
    }

    @Override
    public String getLongUrl(String id) {
        return urls.get(id);
    }
}
