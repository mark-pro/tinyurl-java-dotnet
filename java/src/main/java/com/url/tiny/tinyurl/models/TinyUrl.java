package com.url.tiny.tinyurl.models;

import lombok.Getter;

@Getter
public class TinyUrl {
    
    private final String id;
    private final String longUrl;

    public TinyUrl(String id, String longUrl) {
        this.longUrl = longUrl;
        this.id = id;
    }
}
