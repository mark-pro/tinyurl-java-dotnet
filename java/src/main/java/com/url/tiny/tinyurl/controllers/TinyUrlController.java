package com.url.tiny.tinyurl.controllers;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.url.tiny.tinyurl.models.TinyUrl;
import com.url.tiny.tinyurl.services.TinyService;

@RestController
@RequestMapping("/tiny")
public class TinyUrlController {

    @Autowired
    private TinyService tinyService;

    @PostMapping(value = "/it")
    public TinyUrl tinyIt(@RequestBody String longUrl) {
        var tu = tinyService.generateTinyUrl(longUrl);
        tinyService.save(tu);
        return tu;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Void> Get(@PathVariable String id) {
        return ResponseEntity.status(HttpStatus.FOUND)
            .location(URI.create(tinyService.getLongUrl(id)))
            .build();
    }
}
