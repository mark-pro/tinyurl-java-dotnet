package com.url.tiny.tinyurl.services;

import com.url.tiny.tinyurl.models.TinyUrl;

public interface TinyService {
    void save(TinyUrl tinyUrl);
    TinyUrl generateTinyUrl(String longUrl);
    String getLongUrl(String id);
}
