package com.url.tiny.tinyurl.services;

import java.util.Iterator;
import java.util.Random;
import java.util.function.Function;

public class Base62 {
    static String CHARACTER_SET =
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static String generate() {
        var bytes = new byte[16];
        new Random().nextBytes(bytes);
        return convert(bytes);
    }

    public static String convert(byte[] bytes) {
        Function<Integer, Iterator<Character>> yield = 
            (Integer number) ->
                new Iterator<Character>() {
                    int lNumber = number;
                    @Override
                    public boolean hasNext() {
                        return lNumber > 0;
                    }

                    @Override
                    public Character next() {
                        var temp = lNumber;
                        lNumber /= 62;
                        return CHARACTER_SET.charAt(temp % 62);
                    }
                    
                };

        var sb = new StringBuilder();
        for (byte b : bytes)
            yield.apply((int)b)
                .forEachRemaining(c -> sb.append(c));

        return sb.toString();
    }

    public static String create(int len) {
        return generate().substring(0, len);
    }
}
