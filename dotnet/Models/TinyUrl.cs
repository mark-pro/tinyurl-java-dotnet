
namespace TinyIt.Models;

public record TinyUrl(string Id, string LongUrl);
