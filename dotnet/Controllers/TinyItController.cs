
using Microsoft.AspNetCore.Mvc;
using TinyIt.Models;
using TinyIt.Services;

namespace TinyIt.Controllers;

[ApiController, Route("tiny")]
public class TinyItController : ControllerBase {
    private readonly ITinyService _tinyService;

    public TinyItController(ITinyService tinyService) =>
        _tinyService = tinyService;

    [HttpPost("it")]
    public TinyUrl TinyIt([FromBody] string longUrl) {
        var tu = _tinyService.GenerateTinyUrl(longUrl);
        _tinyService.Save(tu);
        return tu;
    }

    [HttpGet("{id}")]
    public RedirectResult Get(string id) =>
        Redirect(_tinyService.GetLongUrl(id));
}