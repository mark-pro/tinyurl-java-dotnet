using System.Security.Cryptography;
using System.Text;

namespace TinyIt.Services;

public struct Base62 {
    private const string CHARACTER_SET = 
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static string Generate() =>
        Convert(RandomNumberGenerator.GetBytes(16));

    public static string Convert(IEnumerable<byte> bytes) =>
        Convert(bytes.ToArray());

    public static string Convert(byte[] bytes) {
        static IEnumerable<char> yield(int number) {
            do {
                yield return CHARACTER_SET[number % 62];
            } while ((number /= 62) > 0);
        }

        var chars = bytes.Select(System.Convert.ToInt32).SelectMany(yield);
        return string.Join("", chars);
    }

    public static string Create(int len) =>
        Generate()[0..len];
}