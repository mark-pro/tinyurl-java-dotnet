
using TinyIt.Models;

namespace TinyIt.Services;

public interface ITinyService {
    void Save(TinyUrl tinyUrl);
    TinyUrl GenerateTinyUrl(string longUrl);
    String GetLongUrl(string id);
}

struct TinyService : ITinyService {

    readonly static Dictionary<string, string> Urls = new();

    public TinyUrl GenerateTinyUrl(string longUrl) =>
        new(Base62.Create(7), longUrl);

    public string GetLongUrl(string id) =>
        Urls[id];

    public void Save(TinyUrl tinyUrl) =>
        Urls[tinyUrl.Id] = tinyUrl.LongUrl;
}